# import nltk
import nltk

# import pickle
import pickle

# Functions for vectorizing and getting labels/data
from dataset import vectorize_tsv_data, get_column_from_tsv_data

# We will train a KNN classifier
from sklearn.neighbors import KNeighborsClassifier

# train data
train_data='C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/__train.tsv'
#train_data='C:/Users/galanisd/Desktop/LangTech/sentiment-analysis-on-movie-reviews/__test.tsv'

# Create vectors
feature_vectors = vectorize_tsv_data(train_data, 'Phrase', 'train', './model/vocab', 10000)
feature_labels = get_column_from_tsv_data(train_data, 'Sentiment')

# Train a KNN model
# Default K=5
knn_model = KNeighborsClassifier().fit(feature_vectors, feature_labels)
# set K of K-NN
#knn_model = KNeighborsClassifier(n_neighbors = 5).fit(feature_vectors, feature_labels)

# Save the model to disk
filename = './model/knn.model'
pickle.dump(knn_model, open(filename, 'wb'))
