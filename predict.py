# This is for reading from disk
import pickle

# Used to create vectors etc
from dataset import vectorize_tsv_data, get_column_from_tsv_data

# Used for evaluation.
from sklearn import metrics

# Load model
filename = './model/knn.model'
loaded_model = pickle.load(open(filename, 'rb'))
print('model loaded')

test_data_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/__test.tsv'

# load test data -> create feature   vectors for them
feature_vectors = vectorize_tsv_data(test_data_path, 'Phrase', 'predict', './model/vocab', 10000)

# Predict
print('predict')
predictions = loaded_model.predict(feature_vectors)

# Present results
# Get phrases
phrases = get_column_from_tsv_data(test_data_path, 'Phrase')
length = len(phrases)

# Print Prediction and Phrase side by side
print("{0:5}{1:15}{2:200}".format("Num","Prediction","Phrase"))
print('------------------------------------------------------------------------------------------------------------')
for i in range(length):
    print ("{0:5}{1:15}{2:200}".format(str(i), str(predictions[i]), phrases[i]))

# Evaluate

# Get correct labels
correct_labels = get_column_from_tsv_data(test_data_path, 'Sentiment')

# Calculate accuracy
acc = metrics.accuracy_score(correct_labels, predictions)
print("Accuracy:", acc)

# Confusion matrix
matrix = metrics.confusion_matrix(correct_labels, predictions)
print(matrix)
