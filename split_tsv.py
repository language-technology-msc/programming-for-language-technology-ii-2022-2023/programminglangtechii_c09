# ------------------------------
source_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/train.tsv'
test_data_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/___test.tsv'
train_data_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/___train.tsv'
split = 5000
# ------------------------------

src = open(source_path, "r")
train = open(train_data_path, "w")
test = open(test_data_path, "w")

# read first line
first_line = src.readline()

train.write(first_line)
test.write(first_line)

current = 1;

# read second line
next_line = src.readline()

while next_line:
    print(current)
    if current <= split:
        test.write(next_line)
    else:
        train.write(next_line)

    current = current + 1
    next_line = src.readline()

test.flush()
train.flush()
test.close()
train.close()




