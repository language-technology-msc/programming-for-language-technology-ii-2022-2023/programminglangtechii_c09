from nltk.probability import FreqDist
from dataset import vectorize_tsv_data, get_column_from_tsv_data

train_data_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/__train.tsv'
test_data_path = 'C:/Users/galanisd/Desktop/TeachingLanguageTech/21_22LangTech/sentiment-analysis-on-movie-reviews/__test.tsv'

training_labels = get_column_from_tsv_data(train_data_path, 'Sentiment')
test_labels = get_column_from_tsv_data(test_data_path, 'Sentiment')

# Calculate frequencies with FreqDist
fdist_train = FreqDist(training_labels)
fdist_most_common_train = fdist_train.most_common(5)
print(fdist_most_common_train)


fdist_test = FreqDist(test_labels)
fdist_most_common_test = fdist_test.most_common(5)
print(fdist_most_common_test)